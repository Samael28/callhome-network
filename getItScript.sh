#!/bin/bash

scp -r samael@media-proxy1.local:/etc/freeswitch /home/samael/GitRepo/media-proxy1.local/
scp -r samael@sip-reg1.local:/etc/opensips /home/samael/GitRepo/sip-reg1.local/
scp -r samael@media-server1.local:/etc/asterisk /home/samael/GitRepo/media-server1.local/
